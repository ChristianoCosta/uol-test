const axios = require('axios');

const getUrl = 'http://localhost:3000/users';
const storeUrl = 'http://localhost:3000/users';
const deleteUrl = 'http://localhost:3000/users/';

const userComponent = (user) => {
    const elLi = document.createElement('li');
    const elButton = document.createElement('button');
    elButton.setAttribute('class','destroy');
    elButton.setAttribute('value',user.id);
    elButton.innerHTML = 'Excluir';

    elLi.innerHTML = user.name + ' '; 
    elLi.appendChild(elButton)

    elButton.addEventListener('click', event => {
        destroy(elButton);
    });
    
    return elLi;
}

const add = (user) => {
    const list = document.getElementById('list');
    list.appendChild(userComponent(user));
}

const store = () => {
    const input = document.getElementById('name');

    axios.post(storeUrl, {
            name: input.value
        })
        .then(function (response) {
            add(response.data);
        })
        .catch(function (error) {
            console.log(error);
        });
}

const destroy = (el) => {
    let id = el.value;

    axios.delete(deleteUrl+id)
        .then(function (response) {
            el.closest('li').remove();
        })
        .catch(function (error) {
            console.log(error);
        });
}

const all = (el) => {
    axios.get(getUrl)
        .then(function (response) {
            response.data.forEach(function(user) {
                add(user);
            });
        })
        .catch(function (error) {
            console.log(error);
        });
}

all();

document.getElementById("store").addEventListener("click", () => {
    store();
});
