<?php 

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Commons\CommonRepo as Repo;
use App\{Student};

/**
 * Students repository
 */
class StudentsRepo extends Repo
{
    /**
     * @param Student $model
     */
    public function __construct(Student $model)
    {
        $this->model = $model;
    }

}