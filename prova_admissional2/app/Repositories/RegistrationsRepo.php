<?php 

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Commons\CommonRepo as Repo;
use App\{Registration};

/**
 * Cources repository
 */
class RegistrationsRepo extends Repo
{
    /**
     * @param Registration $model
     */
    public function __construct(Registration $model)
    {
        $this->model = $model;
    }

    /**
     * Activate or Inactivate student
     * @param Student $student
     * @return Student
     */
    public function activate(Registration $registration): bool
    {
        return $registration->update(['is_active' => !$registration->is_active]);
    }

}