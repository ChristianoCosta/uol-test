<?php 

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Commons\CommonRepo as BaseRepo;
use App\{Course};

/**
 * Courses repository
 */
class CoursesRepo extends BaseRepo
{
    /**
     * @param Course $model
     */
    public function __construct(Course $model)
    {
        $this->model = $model;
    }

}