<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Registration extends Model
{
    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'course_id' => 'required',
        'student_id' => 'required',
    ];

    /**
     * Validation messages
     * @var array
     */
    public $messages = [
        'course_id.required' => 'Esta informação é obrigatória',
        'student_id.required' => 'Esta informação é obrigatória',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'admissioned_at',
    ];

    protected $fillable = [
        'course_id',
        'student_id',
        'is_active',
        'admissioned_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(){
        return $this->belongsTo(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student(){
        return $this->belongsTo(Student::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departament(){
        return $this->belongsTo(Departament::class);
    }

    /**
     * Validate input data
     * @param  array $input
     * @return Validator
     */
    public function validate(array $input)
    {
        return Validator::make($input, $this->rules, $this->messages);
    }
}
