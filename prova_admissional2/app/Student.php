<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Student extends Model
{   
    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'password' => 'min:6',
        'email' => 'required|unique:students,email',
    ];

    /**
     * Validation messages
     * @var array
     */
    public $messages = [
        'password.min' => 'Senha deve conter no mínimo 6 caracteres',
        'email.unique' => 'Email informado já existe',
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses(){
        return $this->belongsToMany(Course::class, 'registrations');
    }

    /**
     * Validate input data
     * @param  array $input
     * @param  Model $model | null
     * @return Validator
     */
    public function validate(array $input)
    {
        $rules = $this->rules;
        if($this->id)
        {
            $rules['email'] = $rules['email'].','.$this->id;
        }

        return Validator::make($input, $rules, $this->messages);
    }
}
