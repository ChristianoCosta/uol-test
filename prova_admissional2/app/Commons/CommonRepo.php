<?php namespace App\Commons;

use Arr;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use App\Utils\Str;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Route;


/**
 * CommomRepo 
 */
abstract class CommonRepo {


    public function __construct(Model $model )
    {
        $this->model = $model;
    }

    /**
     * Find resource.
     * @param mixed $id
     * @return Model
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Store a new resource
     *
     * @param array $input
     * @return Model
     */
    public function store(array $input)
    {
        $attrs = Arr::only($input, $this->model->getFillable());
        $model = new $this->model($attrs);

        if (!$model->save())
        {
            return null;
        };

        $this->created($model, $input);

        return $model;
    }

    /**
     * Update a resource
     *
     * @param array $input
     * @param Model $model
     * @return Model
     */
    public function update(array $input, Model $model)
    {   
        $attrs = Arr::only($input, $this->model->getFillable());
        $model->fill($attrs);

        if (!$model->save())
        {
            return null;
        };

        $this->updated($model, $input);

        return $model;
    }

    /**
     * Delete a model
     *
     * @param Model $model
     * @param bool $force
     * @return boolean
     */
    public function delete(Model $model, $force = false): bool
    {
        try
        {
            if (!$force)
            {
                $model->delete();
            }
            else
            {
                $model->forceDelete();
            };

            $this->deleted($model);
    
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    protected function created(Model $model, array $input): void
    {
        # code...
    }

    protected function updated(Model $model, array $input): void
    {
        # code...
    }

    protected function deleted(Model $model): void
    {
        # code...
    }

}