<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Carbon\Carbon;

class Course extends Model
{
    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'name' => 'required|unique:courses,name',
    ];

    /**
     * Validation messages
     * @var array
     */
    public $messages = [
        'name.unique' => 'Nome do curso informado já existe',
    ];

    protected $casts = [
        'begined_at' => 'datetime:Y-m-d H:i',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'begined_at',
    ];

    protected $fillable = [
        'name',
        'begined_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function students(){
        return $this->belongsToMany(Student::class, 'registrations');
    }

    public function setBeginedAtAttribute( $value ) 
    {
        $this->attributes['begined_at'] = (new Carbon($value))->format('Y-m-d H:i:s');
    }

    /**
     * Validate input data
     * @param  array $input
     * @return Validator
     */
    public function validate(array $input)
    {
        return Validator::make($input, $this->rules, $this->messages);
    }
}
