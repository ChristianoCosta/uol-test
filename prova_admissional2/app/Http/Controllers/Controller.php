<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $this->model->paginate(10);

        return view($this->resources['index'], get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->model;

        return view($this->resources['create'], get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validator = $this->model->validate($request->all(), $this->model);

            if ($validator->fails())
            {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $result = $this->repo->store($request->all());

            if (!$result)
            {
                return redirect()->back()
                            ->withInput()
                            ->with(['error' => trans('def.nok')]);
            }

            return redirect()->route($this->resources['index'])->with(['success' => trans('def.ok')]);
        }
        catch(Exception $e)
        {
            return redirect()->back()
                            ->withInput()
                            ->with(['error' => trans('def.nok')]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->model->findOrFail($id);

        return view($this->resources['edit'], get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        
        try
        {
            $validator = $this->model->validate($request->all(), $this->model);

            if ($validator->fails())
            {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $result = $this->repo->update($input, $this->model->findOrFail($id));

            if (!$result)
            {
                return redirect()->back()
                            ->withInput()
                            ->with(['error' => trans('def.nok')]);
            }

            return redirect()->route($this->resources['index'])->with(['success' => trans('def.ok')]);
        }
        catch(Exception $e)
        {
            return redirect()->back()
                            ->withInput()
                            ->with(['error' => trans('def.nok')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            $this->repo->delete($this->model->findOrFail($id));

            return redirect()->route($this->resources['index'])->with(['success' => trans('def.ok')]);
        }
        catch(Exception $e)
        {
            return redirect()->back()
                            ->withInput()
                            ->with(['error' => trans('def.nok')]);
        }
    }
}
