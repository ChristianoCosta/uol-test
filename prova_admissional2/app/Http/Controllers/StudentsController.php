<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Repositories\StudentsRepo;

class StudentsController extends Controller
{
    /**
     * Resources
     */
    protected $resources = [
        'index' => 'students.index',
        'create' => 'students.create',
        'edit' => 'students.edit',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Student $model, StudentsRepo $repo)
    {
        $this->model = $model;
        $this->repo = $repo;
    }
}
