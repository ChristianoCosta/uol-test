<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
    Registration,
    Student,
    Course
};
use App\Repositories\RegistrationsRepo;

class RegistrationsController extends Controller
{
    /**
     * Resources
     */
    protected $resources = [
        'index' => 'registrations.index',
        'create' => 'registrations.create',
        'edit' => 'registrations.edit',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Registration $model, RegistrationsRepo $repo)
    {
        $this->model = $model;
        $this->repo = $repo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = $this->model;
        $courses = Course::all();
        $students = Student::all();

        return view($this->resources['create'], get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->model->findOrFail($id);
        $courses = Course::all();
        $students = Student::all();

        return view($this->resources['edit'], get_defined_vars());
    }

    public function activate(Registration $registration)
    {
        try 
        {
            $this->repo->activate($registration);

            return redirect()->route($this->resources['index'])->with(['success' => trans('def.ok')]);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()
                            ->withInput();
        }
    }
}
