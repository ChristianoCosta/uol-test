<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Repositories\CoursesRepo;

class CoursesController extends Controller
{
    /**
     * Resources
     */
    protected $resources = [
        'index' => 'courses.index',
        'create' => 'courses.create',
        'edit' => 'courses.edit',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Course $model, CoursesRepo $repo)
    {
        $this->model = $model;
        $this->repo = $repo;
    }
}
