<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index')->name('index');

// Route::get('/user', [$ns, 'index']);

// Alunos
$ns = 'StudentsController';
$path = 'students';
Route::prefix($path)->name($path.'.')->group(function () use ($ns) {
});
Route::resource($path, $ns)->except(['show']);

// Matricula
$ns = 'RegistrationsController';
$path = 'registrations';
Route::prefix($path)->name($path.'.')->group(function () use ($ns) {
    Route::any('/{registration}/activate', $ns.'@activate')->name('activate');
});
Route::resource($path, $ns)->except(['show']);

// Cursos
$ns = 'CoursesController';
$path = 'courses';
Route::prefix($path)->name($path.'.')->group(function () use ($ns) {
});
Route::resource($path, $ns)->except(['show']);