@extends('index')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Matriculas</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-end">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Matriculas</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <a href="{{ route('registrations.create') }}">
                            <i aria-hidden="true" class="fa fa-plus"></i> Novo
                        </a>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <td>Aluno</td>
                                    <td>Curso</td>
                                    <td>Status</td>
                                    <td>Admitido em</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items as $registration)
                                <tr>
                                    <td>{{ $registration->student->name }}</td>
                                    <td>{{ $registration->course->name }}</td>
                                    <td>{{ $registration->created_at->format('d/m/Y') }}</td>
                                    <td>
                                        @if(!$registration->is_active)
                                        Intivo
                                        @else
                                        Ativo
                                        @endif
                                    </td>
                                    
                                    <td class="text-right py-0 align-middle">
                                        <div class="btn-group">
                                            <a class="btn btn-link" href="{{ route('registrations.edit', $registration) }}">Editar</a>
                                            <a class="btn btn-link" href="{{ route('registrations.activate', $registration) }}">
                                                @if($registration->is_active)
                                                Inativar
                                                @else
                                                Ativar
                                                @endif
                                            </a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $items->links('layout.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
</section>
@endsection