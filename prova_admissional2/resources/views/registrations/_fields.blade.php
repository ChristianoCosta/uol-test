<div class="row">
    <div class="col-md-6">
        <div class="mb-3">
            <label class="required" for="student_id">Aluno: </label>
            <select id="student_id" name="student_id" class="form-control @error('student_id') is-invalid @enderror select2" required>
                <option value="">[ Selecione ]</option>
                @foreach($students as $student)
                    <option value="{{ $student->id }}" @if($student->id == $item->student_id) selected @endif>{{ $student->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('student_id'))
            <span class="help-block">
                <strong>{{ $errors->first('student_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="mb-3">
            <label class="required" for="course_id">Curso: </label>
            <select id="course_id" name="course_id" class="form-control @error('course_id') is-invalid @enderror select2" required>
                <option value="">[ Selecione ]</option>
                @foreach($courses as $course)
                    <option value="{{ $course->id }}" @if($course->id == $item->course_id) selected @endif>{{ $course->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('course_id'))
            <span class="help-block">
                <strong>{{ $errors->first('course_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="mt-3">
            <input type="checkbox" id="all_viewers" name="is_active" class="checkbox" value="1" @if($item->is_active) checked @endif>
            <label for="is_active">Curso ativo</label>
        </div>
    </div>
</div>