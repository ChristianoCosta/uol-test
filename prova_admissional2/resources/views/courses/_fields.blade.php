<div class="row">
    <div class="col-md-6">
        <div class="mb-3">
            <label class="required form-label" for="name">Nome:</label>
            <input required id="name" type="text" class="form-control" name="name"  value="{{ old('name', $item->name) }}">
            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="mb-3">
            <label class="required form-label" for="begined_at">Inicio em:</label>
            <input required id="begined_at" type="datetime-local" class="form-control" name="begined_at"  value="{{ old('begined_at', $item->begined_at ? $item->begined_at->toDateTimeLocalString() : '' ) }}">
            @if ($errors->has('begined_at'))
            <span class="help-block">
                <strong>{{ $errors->first('begined_at') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-6 mt-1">
        <h3>Alunos</h3>
        @if($item->exists())
        <ol class="list-group list-group-numbered">
            @foreach ($item->students as $studant)
            <li class="list-group-item">{{ $studant->name }}</li>
                
            @endforeach
        </ol>
        @endif
    </div>
</div>