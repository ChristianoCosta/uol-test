@extends('index')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Cursos</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-end">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('courses.index') }}">Cursos</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <a href="{{ route('courses.index') }}">
                            <i aria-hidden="true" class="fa fa-arrow-left"></i>
                        </a>
                    </div>
                    <div class="card-body p-4">
                        <form class="form-validation" role="form" method="POST" action="{{ route('courses.update', $item) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')

                            @include('courses._fields')
                            
                            <div class="mt-4">
                                <button id="btn-submit" type="submit" class="btn btn-primary mb-0">Salvar</button>
                                <a href="{{ route('courses.index') }}" class="btn btn-link">Cancelar</a>
                            </div>
                        </form>
                        <form method="POST" action="{{ route('courses.destroy', $item) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-danger btn btn-default float-end">
                                <i aria-hidden="true" class="text-danger fa fa-trash"></i> Excluir
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection