@extends('index')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Cursos</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-end">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Cursos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <a href="{{ route('courses.create') }}">
                            <i aria-hidden="true" class="fa fa-plus"></i> Novo
                        </a>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <td>Nome</td>
                                    <td>Iniciado em</td>
                                    <td>Alunos</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items as $course)
                                <tr>
                                    <td>{{ $course->name }}</td>
                                    <td>{{ $course->begined_at->format('d/m/Y H:i') }}</td>
                                    <td>{{ $course->students->count() }}</td>
                                    
                                    <td class="text-right py-0 align-middle">
                                        <div class="btn-group">
                                            <a class="btn btn-link" href="{{ route('courses.edit', $course) }}">Editar</a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $items->links('layout.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
</section>
@endsection