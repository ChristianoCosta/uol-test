@extends('index')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Alunos</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-end">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="breadcrumb-item active">Alunos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <a href="{{ route('students.create') }}">
                            <i aria-hidden="true" class="fa fa-plus"></i> Novo
                        </a>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <td>Nome</td>
                                    <td>Email</td>
                                    <td>Criado em</td>
                                    <td>Cursos</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items as $student)
                                <tr>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->email }}</td>
                                    <td>{{ $student->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $student->courses->count() }}</td>
                                    
                                    <td class="text-right py-0 align-middle">
                                        <div class="btn-group">
                                            <a class="btn btn-link" href="{{ route('students.edit', $student) }}">Editar</a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $items->links('layout.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
</section>
@endsection