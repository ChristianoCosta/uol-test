<div class="row">
    <div class="col-md-6">
        <div class="mb-3">
            <label class="required form-label" for="name">Nome:</label>
            <input required id="name" type="text" class="form-control" name="name"  value="{{ old('name', $item->name) }}">
            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="mb-3">
            <label class="required form-label" for="name">Email:</label>
            <input required id="email" type="email" class="form-control" name="email"  value="{{ old('email', $item->email) }}">
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="mb-3">
            <label class="required form-label" for="password">Senha:</label>
            <input required id="password" type="senha" class="form-control" name="password"  value="{{ old('password', $item->password) }}">
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-6 mt-1">
        <h3>Cursos</h3>
        @if($item->exists())
        <ol class="list-group list-group-numbered">
            @foreach ($item->courses as $course)
            <li class="list-group-item">{{ $course->name }}</li>
            @endforeach
        </ol>
        @endif
    </div>
</div>
