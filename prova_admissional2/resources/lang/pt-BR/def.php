<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Language Lines
    |--------------------------------------------------------------------------
    */

    'nok' => 'A operação não pode ser concluída. Por favor tente novamente.',
    'ok' => 'A operação foi concluída com sucesso!',

    
];